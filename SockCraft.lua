-- vars
local print, gui, require

-- functions

-- loads a lib from /usr/lib
local function require_lib(name)
  local tEnv = {}
  setmetatable(tEnv, {__index = _G})
  local f, err = loadfile("/usr/lib/"..name)
  if not f then
    return nil, err
  end
  setfenv(f, tEnv)
  local s, m = pcall(f)
  if not s then
    return nil, m
  end
  return tEnv
end

local function init()
  local msg
  if KilOS and System then
    platform, msg = require_lib("KiloCrossplat/KilOS.lua")
  else
    platform, msg = require_lib("KiloCrossplat/CraftOS.lua")
  end
  if not platform then
    error("Could not load Platform specific code: "..(msg or "<Unknown>")) 
  end
  require = function(...)
    local r, m = platform.require(...)
    if not r then error("Require failed: "..(m or "<unknown>"), 1) end
    return r
  end
  print = platform.print
  irc, msg = require("irc")
  gui = require("KiloGui")
end

local function run()
  init()
  
end
run()