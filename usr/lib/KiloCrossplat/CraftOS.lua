print = _G.print
peripheral = _G.peripheral

function require(name)
  local tEnv = {}
  setmetatable(tEnv, {__index = _G})
  local f, err = loadfile("/usr/lib/"..name..".lua")
  if not f then
    return nil, err
  end
  setfenv(f, tEnv)
  f()
  return tEnv
end

function getSides()
  return {"top", "bottom", "left", "right", "front", "back"}
end