IrcConnection = {}
IrcUser = {}
IrcChannel = {}

local connections = {}

-----------------------------------------------------------------

local serverreturns = {}

local function split(self, sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  self:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

local function callHandler(self, name, ...)
  if self.handlers[name] then
    for k, v in ipairs(self.handlers[name]) do
      v(self, ...)
    end
  end
end

function serverreturns.join(con, nick, ident, host, params, more)
  local channel, user
  channel = con:getOrCreateChannel(params[2])
  user = con:getOrCreateUser(nick, ident, host)
  callHandler(con, "join", user, channel) 
end

-- whois
serverreturns['311'] = function(con, nick, ident, host, params, more) 
  -- nick nick ident host | real
  print(textutils.serialize({nick, params}))
  user = con:getOrCreateUserWithoutWhois(params[3])
  user.ident = params[4]
  user.host = params[5]
  user.nickserv = nil
  user.channels = {}
end

-- whois channels
serverreturns['319'] = function(con, nick, ident, host, params, more) 
  -- nick nick | channels
  user = con:getOrCreateUserWithoutWhois(params[3])
  for k, v in ipairs(split(more)) do
    table.insert(user.channels, con:getOrCreateChannel(v))
  end
end

-- whois server
serverreturns['312'] = function(con, nick, ident, host, params, more) 
  -- nick nick server | Description
  user = con:getOrCreateUserWithoutWhois(params[3])
  user.server = params[4]
end

-- whois idle/signon
serverreturns['317'] = function(con, nick, ident, host, params, more) 
  -- nick nick idletime logintime | Info
  user = con:getOrCreateUserWithoutWhois(params[3])
  user.idleTime = tonumber(params[4])
end

-- whois nickserv
serverreturns['330'] = function(con, nick, ident, host, params, more) 
  -- nick nick account | Info
  user = con:getOrCreateUserWithoutWhois(params[3])
  user.nickserv = params[4]
end

-- whois end
serverreturns['318'] = function(con, nick, ident, host, params, more) 
  -- nick nick | Info
  user = con:getOrCreateUserWithoutWhois(params[3])
  callHandler(con, "whoisDone", user)
end

-- names
serverreturns['353'] = function(con, nick, ident, host, params, more) 
  -- nick nick | Info
  channel = con:getOrCreateChannel(params[4])
  if channel.namesDone or not channel.users then
    channel.namesDone = false
    channel.users = {}
  end
  for k, v in ipairs(split(more, " ")) do
    table.insert(channel.users, con:getOrCreateUser(nick, ident, host))
  end
end

-- names end
serverreturns['366'] = function(con, nick, ident, host, params, more) 
  channel = con:getOrCreateChannel(params[3])
  channel.namesDone = true
end

local function trim(s)
  return s:match "^%s*(.-)%s*$"
end

function IrcConnection.sendRawLine(self, line)
  print("< "..line)
  self.webif.writeLine(self.handle, line)
end

function IrcConnection.new(handle, side)
  local t = {
    closed = true,
    handlers = {},
    waitingForPing = true,
    channels = {},
    users = {}
  }
  setmetatable(t, IrcConnection)
  
  if not side then
    sides = redstone.getSides()
    for k, v in ipairs(sides) do
      if peripheral.getType(v) == "WebInterface" then
        t.webif = peripheral.wrap(v)
        break
      end
    end
  else
    t.webif = peripheral.wrap(side)
  end
  if not t.webif then
    error("Faliled to locate/wrap WebInterface")
  end
  
  t.webif.disconnect(handle)
  t.webif.unregisterHandle(handle)
  code, msg = t.webif.registerHandle(handle)
  if code ~= 0 then
    error("Failed to create Handle: "..msg)
  end
  t.handle = handle
  
  connections[t.handle] = t
  return t
end

function IrcConnection.connect(self, ip, port)
  if not self.closed then
    error("Already Connected")
  end
  local code, msg = self.webif.connect(self.handle, ip, port, false)
  if code ~= 0 then
    error("Failed to connect: "..msg)
  end
  self.webif.subscribeEvent(self.handle, "text")
  self.closed = false
  self.host = ip
  self.port = port
end

function IrcConnection.logIn(self, nick, ident, realname, serverpass)
  if serverpass then
    self:sendRawLine("PASS "..serverpass)
  end
  self:sendRawLine("NICK "..(nick or "KiloIrcUser"))
  self:sendRawLine("USER "..(ident or "KiloIrc").." "..self.host.." 0 :"..(realname or "KiloIrc - The Computercraft IRC Lib: http://www.kilobyte.r-o-o-t.net/?p=kiloirc"))
end

function IrcConnection:quit(message)
  self:sendRawLine("QUIT :"..(message or "KiloIrc - http://www.kilobyte.r-o-o-t.net/?p=kiloirc"))
  self.closed = true
end

function IrcConnection:join(channel, pass)
  if self.waitingForPing then
    error("Did not get a ping yet")
  end
  local c = channel
  if type(channel) == "table" then
    c = ""
    for k, v in ipairs(channel) do
      if c ~= "" then c = c.."," end
      c = c..v
    end
  end
  
  local p = pass
  if type(pass) == "table" then
    p = ""
    for k, v in ipairs(channel) do
      if p ~= "" then p = p.."," end
      p = p..v
    end
  end
  
  self:sendRawLine("JOIN "..c.." "..(p or ""))
end

function IrcConnection:registerHandler(name, handler)
  if not self.handlers[name] then
    self.handlers[name] ={}
  end
  table.insert(self.handlers[name], handler)
end

function IrcConnection:getOrCreateChannel(name)
  if self.channels[name:lower()] then
    return self.channels[name:lower()]
  end
  local c = IrcChannel.new(self, name)
  self.channels[name:lower()] = c
  return c
end

function IrcConnection:getUser(nick)
  if not nick then return end
  return self.users[nick:lower()]
end

function IrcConnection:getOrCreateUserWithoutWhois(nick, ident, host)
  local user = self:getUser()
  if user then return user end
  user = IrcUser.new(self, nick, ident, host)
  self.users[nick:lower()] = user
  return user
end

function IrcConnection:getOrCreateUser(nick, ident, host)
  local user = self:getUser()
  if user then return user end
  user = IrcUser.new(self, nick, ident, host)
  if host ~= "*" then
    self:sendRawLine("WHOIS "..nick)
    self.users[nick:lower()] = user
  end
  return user
end

IrcConnection.__index = IrcConnection

----

function IrcConnection:onLine(line)
  if line:sub(1, 6):upper() == "PING :" then
    print("test")
    self:sendRawLine("PONG :"..line:sub(7))
    self.waitingForPing = false
    callHandler(self, "loggedin")
    return
  end
  
  --[[local data = split(line, ":")
  data[3] = line:sub((data[1] or ""):len() + (data[2] or ""):len() + 3)
  data = {data[1], data[2], data[3]}
  --print(textutils.serialize(data))
  data2 = split((data[2] or ""), " ")
  --print(textutils.serialize(data2))
  print("'"..data[2].."'")--]]
  
  --print (line)
  local prefix, user, command = line:match("([^%:]*)%:([^ ]*) (.*)")
  local nick, ident, host = IrcUser.parseHostmask(user)
  
  local command_ = split(command, ":")
  
  -- Splitting at the second colon
  if command_[2] then
    command_[2] = command:sub(command_[1]:len() + 2)
    command_[1] = command_[1]:sub(1, -2)
    command_ = {command_[1], command_[2]}
  end
  
  local cmd = split(command_[1], " ")
  local more = command_[2]
  --print(cmd[1])
  if serverreturns[cmd[1]:lower()] then
    print("calling handler")
    serverreturns[cmd[1]:lower()](self, nick, ident, host, cmd, more)
  end
  
end

function IrcConnection:onClosed()
  callHandler(self, "disconnect")
end

-------------------------------------------------------------------------

IrcUser.__index = IrcUser

function IrcUser.new(connection, nick, ident, host)
  local t = {con = connection, nick = nick, ident = ident, host = host, channels = {}}
  setmetatable(t, IrcUser)
  
  
  return t
end

function IrcUser.parseHostmask(mask)
  local nick, rest = unpack(split(mask, "!"))
  if not rest then rest = "*@*" end
  local ident, host = unpack(split(rest, "@"))
  return nick, ident, host
end

-------------------------------------------------------------------------

IrcChannel.__index = IrcChannel

function IrcChannel.new(connection, name)
  local t = {con = connection, name = name}
  setmetatable(t, IrcChannel)
  return t
end

-------------------------------------------------------------------------

function onEvent(event, handle, p1)
  if event == "socket_line_received" then
    if connections[handle] and p1 ~= "" then
      if p1:sub(-2, -1) == "\r\n" then
        p1 = p1:sub(1, -3)
      end
      connections[handle]:onLine(p1)
    else
      return false, "Couldn't handle event: Invalid handle"
    end
  elseif event == "socket_remote_closed" then
    if connections[handle] then
      connections[handle]:onClosed()
    end
  end
end